//
//  NewTextViewController.swift
//  WatsonService
//
//  Created by Сергей on 28.08.2018.
//  Copyright © 2018 yo. All rights reserved.
//

import UIKit

class NewTextViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return languages.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return languages[row].languages
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
    

    @IBOutlet weak var languageListPickerView: UIPickerView!
    
    var languages = [ListViewModel]()
    var text = [String]()
    
    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()

        // Do any additional setup after loading the view.
    }
    
    fileprivate func fetchData() {
        NetworkManager.shared.getLanguageList(success: { (value) in
            self.languages = value?.map({ return ListViewModel(list: $0)}) ?? []
            self.languageListPickerView.reloadAllComponents()
        }) { (err) in
            print(err)
        }
    }
    
    @IBAction func sendText(_ sender: UIButton) {
        NetworkManager.shared.identy(text: textField.text!, success: { (value1) in
            NetworkManager.shared.translate(text: self.textField.text!, source: value1!, target: self.languages[self.languageListPickerView.selectedRow(inComponent: 0)].key, success: { (value) in
                self.text.append(self.textField.text!)
                self.text.append(value1!)
                self.text.append(value!)
                self.text.append(self.languages[self.languageListPickerView.selectedRow(inComponent: 0)].languages)
                self.performSegue(withIdentifier: "send", sender: self.text)
            }, failure: { (err) in
                print(err)
            })
        }) { (err) in
            print(err)
        }
        
    }
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

//     In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if (segue.identifier == "send") {
            let secondViewController = segue.destination as? DetailViewController
            self.text = sender as! [String]
            secondViewController?.q = self.text
        }
    }

}
