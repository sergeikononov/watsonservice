//
//  DetailViewController.swift
//  WatsonService
//
//  Created by Сергей on 29.08.2018.
//  Copyright © 2018 yo. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var inputedText: UILabel!
    @IBOutlet weak var language: UILabel!
    @IBOutlet weak var translatedText: UILabel!
    @IBOutlet weak var translatedLanguage: UILabel!
    
    var q = [String]()
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        inputedText.text = q[0]
        language.text = q[1]
        translatedLanguage.text = q[3]
        translatedText.text = q[2]
        
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
