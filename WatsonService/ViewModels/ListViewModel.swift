//
//  ListViewModel.swift
//  WatsonService
//
//  Created by Сергей on 29.08.2018.
//  Copyright © 2018 yo. All rights reserved.
//

import Foundation

class ListViewModel {
    var languages: String
    var key: String
    
    
    init(list: LanguageList) {
        self.languages = list.name
        self.key = list.language
    }
    
}
