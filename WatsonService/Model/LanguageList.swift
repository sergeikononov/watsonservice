//
//  LanguageList.swift
//  WatsonService
//
//  Created by Сергей on 28.08.2018.
//  Copyright © 2018 yo. All rights reserved.
//

import Foundation
import SwiftyJSON

struct LanguageList{
    let language: String
    let name: String
    
    static func fromJSON(json: [JSON]) -> [LanguageList]? {
        
        return json.compactMap{ jsonItem -> LanguageList in
            let language = jsonItem["language"].string
            let name = jsonItem["name"].string
            
            return LanguageList(language: language!,
                                name: name!)
        }
    }
}
